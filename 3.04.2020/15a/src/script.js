function stocks (name, marketCap, sales, profit, employees)
{
  this.name = name;
  this.marketCap = marketCap;
  this.sales = sales;
  this.profit = profit;
  this.employees = employees;
  
};




<!--space_--> 
function runFunction(stocks){
  
var comp1 = new stocks( "Microsoft", "$381.7 B", "$86.8 B", "$22.1 B", "128,000");
var comp2 = new stocks( "Symetra Financial", "$2.7 B", "$2.2 B", "$254.4 M", "1,400");
var comp3 = new stocks( "Micron Technology", "$37.6 B", "$16.4 B", "$3.0 B", "30,400");
var comp4 = new stocks( "F5 Networks", "$9.5 B", "$1.7 B", "$311.2 B", "3,834");
var comp5 = new stocks( "Expedia", "$10.8 B", "$5.8 B", "$398.1 B", "18,210");
var comp6 = new stocks( "Nautilus", "$476 M", "$274.4 M", "$18.8 M", "340");
var comp7 = new stocks( "Heritage Financial", "$531 M", "$137.6 M", "$21 M", "748");
var comp8 = new stocks( "Cascade Microtech", "$239 M", "$136 M", "$9.9 M", "449");
var comp9 = new stocks( "Nike", "$83.1 B", "$27.8 B", "$2.7 B", "56,500");
var comp10 = new stocks( "Alaska Air Group", "$7.9 B", "$5.4 B", "$605 M", "13,952");
var wx_data = [comp1, comp2, comp3, comp4, comp5, comp6, comp7, comp8, comp9, comp10];
  
  var table = document.getElementById("table_data");
table.innerHTML =
  `<tr id="ident"> <th scope="col">Name</th><th scope="col"> Market Cap</th><th scope="col">Sales</th><th scope="col">Profit</th><th scope="col">Employees</th>`;

for (var i = 0; i < wx_data.length; i++) {
  table.innerHTML +=
    "<tr><th scope=\"row\">" +
    wx_data[i].name +
    "</th><td>" +
    wx_data[i].marketCap +
    "</th><td>" +
    wx_data[i].sales +
    "</td><td>" +
    wx_data[i].profit +
    "</td><td>" +
    wx_data[i].employees +
    "</td><td>" 
};

  
}

