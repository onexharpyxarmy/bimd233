function football(icon, color, state, conference, overall, lGame) {
  this.icon = icon;
  this.color = color;
  this.state = state;
  this.conference = conference;
  this.overall = overall;
  this.lGame = lGame;
}

var col1 = new football(
  '<img src ="https://cdn.discordapp.com/attachments/677255760916512802/685721731952017410/1200px-Oregon_Ducks_logo.png" width="50px" height="50px">',
  "osu",
  "Oregon",
  "8 - 1",
  "12 - 2",
  "W 28-27 WIS"
);

var col2 = new football(
  '<img src ="https://cdn.discordapp.com/attachments/677255760916512802/685721945227919404/570fab492b4111cb985a58677680e6cf.png" width="50px" height="50px">',
  "cal",
  "California",
  "4 - 5",
  "8 - 5",
  "W 35-20 ILL"
);

var col3 = new football(
  '<img src ="https://cdn.discordapp.com/attachments/677255760916512802/685722134009610290/1200px-Washington_Huskies_logo.png" width="50px" height="50px">',
  "uw",
  "Washington",
  "4 - 5",
  "8 - 5",
  "W 38-7 BSU"
);

var col4 = new football(
  '<img src ="https://cdn.discordapp.com/attachments/677255760916512802/685722291887276085/1200px-Oregon_State_Beavers_logo.png" width="50px" height="50px">',
  "ou",
  "Oregon State",
  "4 - 5",
  "5 - 7",
  "L 10-24 ORE"
);

var col5 = new football(
  '<img src ="https://cdn.discordapp.com/attachments/677255760916512802/685723393005584414/1200px-Washington_State_Cougars_logo.png" width="50px" height="50px">',
  "wsu",
  "WSU",
  "3 - 6",
  "6 - 7",
  "L 21-31 AFA"
);

var col6 = new football(
  '<img src ="https://cdn.discordapp.com/attachments/677255760916512802/685723595087151104/8dbfd3b4fc60761d95b47cfca9153cc6.png" width="50px" height="50px">',
  "su",
  "Stanford",
  "3 - 6",
  "4 - 8",
  "L 24-45 ND"
);

var wx_data = [col1, col2, col3, col4, col5, col6];

var table = document.getElementById("table_data");
table.innerHTML =
  '<tr id="ident"> <th scope="col">North Division</th> <th scope="col">Conference</th> <th scope="col">Overall</th> <th scope="col">Last Game</th></tr>';

for (var i = 0; i < wx_data.length; i++) {
  console.log(wx_data[i].state);
  table.innerHTML +=
    '<tr id = '+ wx_data[i].color +
    '><th scope="row">' +
    wx_data[i].icon +
    " " +
    wx_data[i].state +
    "</th><td>" +
    wx_data[i].conference +
    "</td><td>" +
    wx_data[i].overall +
    "</td><td>" +
    wx_data[i].lGame +
    "</td><td>";
}