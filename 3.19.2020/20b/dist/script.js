function updateData(data) {
  
  var totalResults = data.totalResults;
  var top10 = data.articles.slice(0, 10);
  var list = $("#list").empty();
  $("#summary").html(totalResults + "Results found!");
  
  for (var i = 0; i < top10.length; i++) {
    var wx_data = top10[i];
    list.append(article(wx_data));
    
  }
}

function article(wx_data) {
  var title = wx_data.title;
  var link = wx_data.url;
  return "<li> <a href='" + link + "'>" + title + "</a></li>";
  
}

$(document).ready(function() {
  var URL = "http://newsapi.org/v2/everything?" + "apiKey=908ce6eb70354dde90d6666e1cf0a53d";

  $("#fetch").click(function() {
    var subject = $("#subject").val();
    var url = URL + "&q=" + subject;
    var req = new Request(url);
    fetch(req)
      .then(function(response) {
        return response.json();
      })

      .then(function(data) {
        updateData(data);

        // console.log(data.totalResults);
        // console.log(data.articles[0]);
      // API Key : 908ce6eb70354dde90d6666e1cf0a53d
      });
  });
});