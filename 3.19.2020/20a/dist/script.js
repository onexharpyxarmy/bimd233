var myURL = "https://api.coindesk.com/v1/bpi/currentprice/USD.json";

var priceArray = [];

var graph = Morris.Line({
  element: 'graph',
  data: priceArray,
  xkey: 'time',
  ykeys: ['price'],
  ymin: 'auto',
  ymax: 'auto',
  labels: ['Time', 'Price'],
  parseTime: false,
  hideHover: true
});

var currency = null;

function updatePrice(currency) {
var url = myURL + "/" + currency + ".json";
  $.ajax({
    url : myURL,
    
    success: function(data){
      var bc = JSON.parse(data);
      var price = bc.bpi[currency].rate_float;
      var time = bc.time.updated;
      
      console.log(price);

      var newData = { price: price, time: time };
      updateData(newData);
      graph.setData(priceArray);
      
    }    
  });
}

function updateData(newData) {
  if (priceArray.length >= 10) {
    priceArray.shift();
  }
  priceArray.push(newData);
}

$(document).ready(function() {
  $("#start").click(function() {
    var newCurrency = $("#currency_type").val();
    if (currency != newCurrency) {
      priceArray = [];
      currency = newCurrency;
    }
    setInterval(function() { updatePrice(currency); }, 15000);
  });
});