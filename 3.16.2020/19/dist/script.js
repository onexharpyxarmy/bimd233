const base_url = "https://api.weather.gov/stations/";
const endpoint = "/observations/latest";

// weather update button click
$("#getwx").on("click", function(e) {
  var mystation = $("input").val();
  var myurl = base_url + mystation + endpoint;
  $("input#my-url").val(myurl);

  // clear out any previous data
  $("ul li").each(function() { 
    $(this).remove();
    // enter code to clear each li
  });

  console.log("Cleared Elements of UL");

  // execute AJAX call to get and render data
  $.ajax({
    url: myurl,
    dataType: "json",
    success: function(data) {
      //temperature
      var tempC = data["properties"]["temperature"].value.toFixed(1);
      var tempF = ((tempC * 9) / 5 + 32).toFixed(1);

      var rawMessage = data["properties"]["rawMessage"];
      var textDescription = data["properties"]["textDescription"];
      var windDirection = data["properties"]["windDirection"].value.toFixed(1);
      var windSpeed = (data["properties"]["windSpeed"].value * 1.94384).toFixed(1);
      var relativeHumidity = data["properties"]["relativeHumidity"].value.toFixed(1);
      var icon = data["properties"]["icon"];

      // uncomment this if you want to dump full JSON to textarea
      var myJSON = JSON.stringify(data);
      $("textarea").val(myJSON);

      var str =
        "<li>" 
      + rawMessage 
      + "<li>Current temperature: " 
      + tempC 
      + "C " 
      + tempF 
      + "F"
      +"</li>" 
      + "<li>Humidity: " 
      + relativeHumidity 
      + " %RH" 
      + "</li>" 
      + "</li>" 
      + "<li>Current Wind: " 
      + windDirection 
      + " degrees at " 
      + windSpeed 
      + "kts" 
      + "<li><img>" 
      + "<br/>" 
      + textDescription 
      + "</li>";
      
      
      $("ul").append(str);
      $("ul li").attr("class", "list-group-item");
      $("ul li img").attr("src", icon);

      // add additional code here for the Wind direction, speed, weather contitions and icon image
    }
  });
});