var x1 = 200, x2 = 400, y1 = 200, y2 = 400, radius = 75, angle = Math.PI*2, width = 600, height = 600;

var canvas=document.getElementById("myCanvas");
var context=canvas.getContext("2d");

context.moveTo(x1,0);
context.lineTo(x1, height);
context.moveTo(x2,0);
context.lineTo(x2, height);
context.moveTo(0,y1);
context.lineTo(width, y1);
context.moveTo(0,y2);
context.lineTo(width, y2);
context.stroke();

context.moveTo(x1/2+radius,y1/2);
context.arc(x1/2, y1/2, radius, 0, angle);

context.moveTo(x1/2+radius,y1/2+y2);
context.arc(x1/2, y1/2+y2, radius, 0, angle);

context.moveTo(x1+25, y1 +25);
context.lineTo(x2-25, y2-25);
context.moveTo(x2-25, y1 +25);
context.lineTo(x1+25, y2-25);

context.lineWidth=5;
context.stroke();