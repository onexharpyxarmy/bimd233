function runFunction(){
  
  var running = new Boolean(true);

  var states = ["idle", "s1", "s2", "s3", "s4"];
  var state = 0;

  var cmd;
  var cmdnum;
  
  while(running){
    cmd = prompt(states[state], "next");
    
    //Doing functions
    switch(cmd){
      case "run":
        if(state == 0){
          state = 1;
        }
        break;
        
      case "next":
        if(state != 0){
          state++;
          if(state > 4){
            state = 1;
          }
        }
        break;
        
      case "quit":
      case "exit":
        running = false;
        break;
        
      default:
        console.log("Invalid command!  Use next, exit, or quit.");
        
    }
    
  }
  
}