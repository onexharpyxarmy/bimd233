function Weather( day, high, low, average, visibility, precipitation, wind, humidity) 
{
  this.day = day;
  this.high = high;
  this.low = low;
  this.average = average;
  this.visibility = visibility;
  this.precipitation = precipitation;
  this.wind = wind;
  this.humidity = humidity; 
};

<!-- space --> 
function weatherIcon(str){
  var sunIcon = "";
  
  if(str == "Sunny")
  {
    sunIcon += "<img src =\"https://cdn.discordapp.com/attachments/677255760916512802/684217152483360796/icons-transparent-sun-3.png\" width=\"50px\" height=\"50px\">";
    return sunIcon + str;
  }
  
 else if(str == "Cloudy"){
    sunIcon += "<img src =\"https://cdn.discordapp.com/attachments/677255760916512802/684216657874386972/Cloud_1-512.png\" width=\"50px\" height=\"50px\">";
    return sunIcon + str;
  }
  
 else {
    sunIcon += "<img src =\"https://cdn.discordapp.com/attachments/677255760916512802/684216775994245165/icn-weather-rain-512.png\" width=\"50px\" height=\"50px\">";
    return sunIcon + str;
  }
  
};
<!-- space --> 
function Averages(str){

var highs = [82, 75, 69, 69, 68];
var lows = [55, 52, 52, 48, 51];
  
var avg1 = (highs[0] + lows[0])/2;
var avg2 = (highs[1] + lows[1])/2;
var avg3 = (highs[2] + lows[2])/2;
var avg4 = (highs[3] + lows[3])/2;  
var avg5 = (highs[4] + lows[4])/2;  
  
  if(str == " ")
  {
    return avg1 + str; 
  }
  else if(str == "  ")
  {
    return avg2 + str; 
  }
  else if(str == "   ")
  {
   return avg3 + str; 
  }
  else if(str == "    ")
  {
    return avg4 + str;
  }
  else (str == "     ")
  {
   return avg5 + str;
  }
}

<!--space-->

var day1 = new Weather("Friday", 82, 55, " ", "Sunny", "0%", "N 9 mph", "46%");
var day2 = new Weather("Saturday", 75, 52, "  ", "Cloudy", "20%", "WSW 6 mph", "54%");
var day3 = new Weather("Sunday", 69, 52, "   ", "Showers", "60%", "SSW 8 mph", "70%");
var day4 = new Weather("Monday", 69, 48, "    ", "Cloudy", "20%", "SSW 9 mph", "62%");
var day5 = new Weather("Tuesday", 68, 51, "     ", "Showers", "40%", "SW 7 mph", "57%");
var wx_data = [day1, day2, day3, day4, day5];
<!--space-->

var table = document.getElementById("table_data");
table.innerHTML =
  '<tr id="ident"> <th scope="col">Date</th> <th scope="col">High/Low</th> <th scope="col">Average</th> <th scope="col">Visibility</th> <th scope="col">Precipitation</th> <th scope="col">Wind Speed</th> <th scope="col">Humidity</th></tr>';


<!--space-->
for (var i = 0; i < wx_data.length; i++) {
  table.innerHTML +=
    "<tr><th scope=\"row\">" +
    wx_data[i].day +
    "</th><td>" +
    wx_data[i].high +
    " / " +
    wx_data[i].low +
    "</td><td>" +
    Averages(wx_data[i].average) +
    "</td><td>" +
    weatherIcon(wx_data[i].visibility) +
    "</td><td>" +
    wx_data[i].precipitation +
    "</td><td>" +
    wx_data[i].wind +
    "</td><td>" +
    wx_data[i].humidity +
    "</td></tr>";
};
