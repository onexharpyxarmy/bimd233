var display = document.getElementById("flight1#");

function timeDiff(dep, arrival, divName){
  
  var display = document.getElementById(divName);
  var diff = (arrival.getTime() - dep.getTime()) /3600000;
  diff = diff.toFixed(1);
  display.textContent = diff + " hours";
}

<!--space-->
var flight1 = ["ASA1077", "A319", "Washington Dulles Intl (KIAD)", "San Francisco Intl (KSFO)", "Wed 07:32PM EST", "Wed 10:10PM PST"];

var dep_time1 = new Date('February 28, 2020 19:32:00');
var arrival_time1 = new Date('February 28, 2020 22:10:00');

<!--space-->
var flight2 = ["ASA1088", "A320", "San Francisco Intl (KSFO)", "Washington Dulles Intl (KIAD)", "Wed 03:58PM PST", "Wed 11:28PM PST"];

var dep_time2 = new Date('February 28, 2020 15:58:00');
var arrival_time2 = new Date('February 28, 2020 23:28:00');

<!--space-->
var flight3 = ["ASA1097", "A320", "Washington Dulles Intl (KIAD)", "Los Angeles Intl (KLAX)", "Wed 05:06PM EST", "Wed 7:24PM PST"];

var dep_time3 = new Date('February 28, 2020 17:06:00');
var arrival_time3 = new Date('February 28, 2020 19:24:00');

<!--space-->
var flight4 = ["ASA11", "B739", "Newark Liberty Intl (KEWR)", "Seattle-Tacoma Intl (KSEA)", "Wed 05:00PM EST", "Wed 7:27PM PST"];

var dep_time4 = new Date('February 28, 2020 17:00:00');
var arrival_time4 = new Date('February 28, 2020 19:27:00');

<!--space-->
var flight5 = ["ASA1113", "A320", "Will Rogers World (KOKC)", "Seattle-Tacoma Intl (KSEA)", "Wed 05:40PM CST", "Wed 7:11PM PST"];

var dep_time5 = new Date('February 28, 2020 17:40:00');
var arrival_time5 = new Date('February 28, 2020 19:11:00');

<!--space-->


display.textContent = "Flight " + flight1[0];
display = document.getElementById("flight1type");
display.textContent = "Type " + flight1[1];
display = document.getElementById("flight1from");
display.textContent = "From: " + flight1[2];
display = document.getElementById("flight1to");
display.textContent = "To: " + flight1[3];
display = document.getElementById("dep_time1");
display.textContent = "Departure: " + flight1[4];
display = document.getElementById("arrival_time1");
display.textContent = "Arrival: " + flight1[5];
<!--space-->

display = document.getElementById("flight2#");
display.textContent = "Flight " + flight2[0];
display = document.getElementById("flight2type");
display.textContent = "Type " + flight2[1];
display = document.getElementById("flight2from");
display.textContent = "From: " + flight2[2];
display = document.getElementById("flight2to");
display.textContent = "To: " + flight2[3];
display = document.getElementById("dep_time2");
display.textContent = "Departure: " + flight2[4]
display = document.getElementById("arrival_time2");
display.textContent = "Arrival: " + flight2[5];
<!--space-->

display = document.getElementById("flight3#");
display.textContent = "Flight " + flight3[0];
display = document.getElementById("flight3type");
display.textContent = "Type " + flight3[1];
display = document.getElementById("flight3from");
display.textContent = "From: " + flight3[2];
display = document.getElementById("flight3to");
display.textContent = "To: " + flight3[3];
display = document.getElementById("dep_time3");
display.textContent = "Departure: " + flight3[4]
display = document.getElementById("arrival_time3");
display.textContent = "Arrival: " + flight3[5];
<!--space-->

display = document.getElementById("flight4#");
display.textContent = "Flight " + flight4[0];
display = document.getElementById("flight4type");
display.textContent = "Type " + flight4[1];
display = document.getElementById("flight4from");
display.textContent = "From: " + flight4[2];
display = document.getElementById("flight4to");
display.textContent = "To: " + flight4[3];
display = document.getElementById("dep_time4");
display.textContent = "Departure: " + flight4[4]
display = document.getElementById("arrival_time4");
display.textContent = "Arrival: " + flight4[5];
<!--space-->

display = document.getElementById("flight5#");
display.textContent = "Flight " + flight5[0];
display = document.getElementById("flight5type");
display.textContent = "Type " + flight5[1];
display = document.getElementById("flight5from");
display.textContent = "From: " + flight5[2];
display = document.getElementById("flight5to");
display.textContent = "To: " + flight5[3];
display = document.getElementById("dep_time5");
display.textContent = "Departure: " + flight5[4]
display = document.getElementById("arrival_time5");
display.textContent = "Arrival: " + flight5[5];
<!--space-->