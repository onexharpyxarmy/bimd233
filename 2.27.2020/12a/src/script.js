function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area =pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter =2 * radius;
  var geometries = [area, circumference, diameter];
  return geometries;
}

var display = document.getElementById("display1");
display.textContent = "Circle 1= " + calcCircleGeometries(Math.random()*11);
display = document.getElementById("display2");
display.textContent = "Circle 2= " + calcCircleGeometries(Math.random()*11);
display = document.getElementById("display3");
display.textContent = "Circle 3= " + calcCircleGeometries(Math.random()*11);